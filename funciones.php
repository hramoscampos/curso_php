<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    $num=8;
    
    if ($num<=8) {
        echo "Tienes $num años. Eres niño";
    } else if ($num<=15) {
        echo "Tienes $num años. Eres puber";
    } else if ($num<=17) {
        echo "Tienes $num años. Eres adolescente";
    } else if ($num<=40) {
        echo "Tienes $num años. Eres joven";
    } else if ($num<=100) {
        echo "Tienes $num años. Eres adulto";
    } else{ 
        echo "Tienes $num años. Ingresa una edad correcta";
    }
    
    ?>
</body>
</html>