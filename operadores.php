<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <h1>OPERADORES LÓGICOS</h1>

    <h3>COMPARACIÓN</h3>

    <?php
        
        $num1;
        $num2;
        $num3;
        $num4;
        $num_mayor1;
        $num_mayor2;
        $num_menor1;
        $num_menor2;

        $num1 = 10;    
        $num2 = 35;
        $num3 = 10;
        $num4 = 2;

        //MAYOR

        if ($num1>$num2) {
            $num_mayor1 = $num1;
        }else{
            $num_mayor1 = $num2;
        }

        if ($num_mayor1>$num3){
            $num_mayor2=$num_mayor1;
        }else{
            $num_mayor2=$num3;
        }

        if ($num_mayor2>$num4) {
            
            echo "El mayor de $num1; $num2; $num3 y $num4 es $num_mayor2";
        }else{
            echo "El mayor de $num1; $num2; $num3 y $num4 es $num4";
        }

        //MENOR

        if ($num1<$num2) {
            $num_menor1 = $num1;
        }else{
            $num_menor1= $num2;
        }

        if ($num_menor1<$num3){
            $num_menor2=$num_menor1;
        }else{
            $num_menor2=$num3;
        }

        if ($num_menor2<$num4) {  
            echo " y el menor es $num_menor2</p>";
        }else{
            echo " y el menor es $num4</p>";
        }
    ?>
    
</body>
</html>